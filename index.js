const express = require("express");
const app = express();
const mongoose = require("mongoose");
app.use(express.json());

mongoose.connect(
    "mongodb+srv://ekadyah:ekadyah123@cluster0.eo4x3.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
  );

const placeSchema = new mongoose.Schema({
    nama: String,
    lokasi: String,
  });
  
const placeModel = mongoose.model("place", placeSchema);

app.post("/create", async (req, res) => {
    const nama = req.body.nama;
    const lokasi = req.body.nama;
    await db.place.create({
      nama: nama,
      lokasi: lokasi,
    });
    return res.json({ message: "success create new data place"});
  });
 
app.get('/', async (req, res) => {
    const data = await placeModel.find({});
    return res.json({ message: "success get data"})
})

app.put("/update", async (req, res) => {
    const _id = req.body.id;
    const nama = req.body["nama"];
    const lokasi = req.body["lokasi"];
    const data = await placeModel.updateOne({ _id: id }, { nama: nama, lokasi: lokasi })
    return res.json({ message: "success update  data place"});
  });
 
app.delete("/delete", async (req, res) => {
    const _id = req.query.id;
    const data = await placeModel.findOne({_id: id})
    res.json(`message: success delete id ${_id}}`);
  });

app.listen(5000, () => console.log("app running on port 5000"));
